import Header from "./TryGlass/Header";
import Content from "./TryGlass/Content";


function App() {
  return (
    <div className="wrap bg-dark bg-opacity-50">
      <Header/>
      <Content />
    </div>
  );
}

export default App;
