import React, { Component } from "react";
import GlassItem from "./GlassItem";

export default class GlassList extends Component {

    renderGlasses = () => {
      return this.props.glassesList.map((item) => {
        return (
          <GlassItem key={item.id} data={item} handleTryGlass={this.props.handleTryGlasses} />
        );
      });
    };

  render() {
    return (
      <div className="bottom bg-light py-4 px-5">
        <h4 className="mb-4">Choose your glass</h4>
        <div className="row">{this.renderGlasses()}</div>
      </div>
    );
  }
}
