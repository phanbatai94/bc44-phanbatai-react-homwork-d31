import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <div className='text-center py-3 text-white bg-dark bg-opacity-75'><h1 className='fw-normal'>TRY GLASSES APP ONLINE</h1></div>
    )
  }
}
