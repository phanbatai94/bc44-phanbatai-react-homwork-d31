import React, { Component } from "react";

export default class GlassItem extends Component {
  render() {
    let { data, handleTryGlass } = this.props;
    let {url, name, desc} = data
    return (
      <img
        onClick={() => {
          handleTryGlass(url, name, desc);
        }}
        className="glasses col-2 p-3"
        src={url}
        alt="glass"
      />
    );
  }
}
