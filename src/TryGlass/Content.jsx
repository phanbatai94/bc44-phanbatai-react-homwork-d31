import React, { Component } from "react";
import { glasses } from "./data";
import GlassInfo from "./GlassInfo";
import GlassList from "./GlassList";

export default class TryGlass extends Component {
  state = {
    glasses: glasses,
    url: "",
    name: "",
    desc: "",
    display: "none",
  };

  handleTryGlasses = (url, name, desc) => {
    this.setState({
      url: url,
      name: name,
      desc: desc,
      display: "block",
    });
  };

  render() {
    return (
      <div className="container">
        <GlassInfo url={this.state.url} name={this.state.name} desc={this.state.desc} display={this.state.display} handleTryGlasses={this.handleTryGlasses} />
        <GlassList glassesList={this.state.glasses} handleTryGlasses={this.handleTryGlasses} />
      </div>
    );
  }
}
