import React, { Component } from "react";

export default class GlassInfo extends Component {
  render() {
    let { url, name, desc, display } = this.props;

    return (
      <div className="top py-4 px-5 d-flex justify-content-between">
        <div className="col-3 the-frame">
          <img className="w-100" src="/glassesImage/model.jpg" alt="model" />
          <img className="the-glass" src="./glassesImage/v1.png" alt="themodelglass"/>
          <div className="the-info p-2 bg-black bg-opacity-75">
            <h4 className="text-warning">Trendy Glasses</h4>
            <p className="text-white mb-0">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Impedit cum, voluptas blanditiis nobis minima illum nihil.
            </p>
          </div>
        </div>
        <div className="col-3 the-frame">
          <img className="w-100 the-model" src="/glassesImage/model.jpg" alt="model"/>
          <img
            style={{ display: display }}
            className="the-glass"
            src={url}
            alt="theglass"
          />
          <div style={{ display: display }} className="the-info p-2 bg-black bg-opacity-75">
            <h4 className="text-info">{name}</h4>
            <p className="text-white mb-0">{desc}</p>
          </div>
        </div>
      </div>
    );
  }
}
